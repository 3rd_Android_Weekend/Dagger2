package com.kshrd.dagger2.api;

import com.kshrd.dagger2.entity.response.ArticleResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by pirang on 9/2/17.
 */

public interface ArticleService {

    @GET("articles")
    Single<ArticleResponse> findArticle(@Query("page") int page);

}
