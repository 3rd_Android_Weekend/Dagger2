package com.kshrd.dagger2.data;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

/**
 * Created by pirang on 8/20/17.
 */

public class AppPreferenceHelper implements PreferenceHelper  {

    private static final String USER_PREF = "user_pref";
    private static final String USER_NAME = "user_name";
    private SharedPreferences sharedPreferences;

    @Inject
    public AppPreferenceHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
    }

    @Override
    public void setName(String name) {
        sharedPreferences.edit().putString(USER_NAME, name).apply();
    }

    @Override
    public String getName() {
        return sharedPreferences.getString(USER_NAME, "N/A");
    }

}
