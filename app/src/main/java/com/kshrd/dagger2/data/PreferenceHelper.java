package com.kshrd.dagger2.data;

/**
 * Created by pirang on 8/20/17.
 */

public interface PreferenceHelper {

    void setName(String name);
    String getName();

}
