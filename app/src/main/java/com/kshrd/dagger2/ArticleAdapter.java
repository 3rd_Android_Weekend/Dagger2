package com.kshrd.dagger2;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kshrd.dagger2.entity.Article;
import com.kshrd.dagger2.event.ArticleLoadMoreEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pirang on 9/2/17.
 */

public class ArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int VIEW_ARTICLE = 1;
    private static final int VIEW_LOADING = 0;
    private static final int visibleThreshold = 5;
    private boolean loading = false;

    private List<Article> articleList;

    public ArticleAdapter(RecyclerView recyclerView) {
        articleList = new ArrayList<>();

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = linearLayoutManager.getItemCount();
                int listVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();

                if (!loading && totalItemCount <= (listVisibleItemPosition + visibleThreshold)){
                    // End has been reach
                    loading = true;
                    EventBus.getDefault().post(new ArticleLoadMoreEvent());
                }

            }
        });
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void addItems(List<Article> articleList){
        this.articleList.addAll(articleList);
        notifyDataSetChanged();
    }

    public void addLoading(){
        this.articleList.add(null);
        notifyItemInserted(articleList.size() - 1);
    }

    public void removeLoading(){
        articleList.remove(articleList.size() -1);
        notifyItemRemoved(articleList.size() -1);
    }

    @Override
    public int getItemViewType(int position) {
        return (articleList.get(position) != null) ? VIEW_ARTICLE : VIEW_LOADING ;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ARTICLE){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article, parent, false);
            vh = new ArticleVH(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new LoadingVH(view);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleVH){
            ArticleVH articleVH = (ArticleVH) holder;
            articleVH.tvTitle.setText(articleList.get(position).getTitle());
        } else {
            ((LoadingVH) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    class ArticleVH extends RecyclerView.ViewHolder {

        TextView tvTitle;

        public ArticleVH(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        }
    }

    class LoadingVH extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingVH(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }
}
