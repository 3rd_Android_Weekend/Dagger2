package com.kshrd.dagger2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.kshrd.dagger2.api.ArticleService;
import com.kshrd.dagger2.app.MyApp;
import com.kshrd.dagger2.entity.response.ArticleResponse;
import com.kshrd.dagger2.event.ArticleLoadMoreEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ooooo";
    private CompositeDisposable compositeDisposable;
    private int page = 1;
    private int totalPage;
    private static final int ITEM_PER_PAGE = 15;


    @Inject
    ArticleService articleService;

    RecyclerView rvArticle;
    ArticleAdapter articleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((MyApp) getApplication()).getApplicationComponent().inject(this);
        compositeDisposable = new CompositeDisposable();

        rvArticle = (RecyclerView) findViewById(R.id.rvArticle);
        rvArticle.setLayoutManager(new LinearLayoutManager(this));
        articleAdapter = new ArticleAdapter(rvArticle);
        rvArticle.setAdapter(articleAdapter);

        loadArticle(page);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onArticleLoadMoreEvent(ArticleLoadMoreEvent event){

        if (page == totalPage) return;

        Toast.makeText(this, "Event Fired", Toast.LENGTH_SHORT).show();
        articleAdapter.addLoading();
        loadArticle(++page);

    }

    private void loadArticle(int page) {
        compositeDisposable.add(
                articleService.findArticle(page)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ArticleResponse>() {
                            @Override
                            public void onSuccess(ArticleResponse response) {

                                totalPage = response.getPagination().getTotalPages();

                                if (articleAdapter.isLoading() && (articleAdapter.getArticleList().size() > ITEM_PER_PAGE)){
                                    articleAdapter.removeLoading();
                                }

                                articleAdapter.addItems(response.getArticleList());
                                articleAdapter.setLoading(false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }
                        })
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
        EventBus.getDefault().unregister(this);
    }
}
