package com.kshrd.dagger2.app.di.module;

import android.app.Application;
import android.content.Context;

import com.kshrd.dagger2.app.di.Url;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pirang on 8/20/17.
 */

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    public Context provideApplicationContext(){
        return application.getApplicationContext();
    }

    @Provides
    @Url
    public String provideUrl(){
        return "https://google.com";
    }

    @Provides
    @Named("key")
    public String provideKey(){
        return "key:abc1238374569";
    }

}
