package com.kshrd.dagger2.app;

import android.app.Application;

import com.kshrd.dagger2.app.di.component.ApplicationComponent;
import com.kshrd.dagger2.app.di.component.DaggerApplicationComponent;
import com.kshrd.dagger2.app.di.module.ApplicationModule;

/**
 * Created by pirang on 8/20/17.
 */

public class MyApp extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
