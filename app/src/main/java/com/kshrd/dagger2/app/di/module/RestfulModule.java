package com.kshrd.dagger2.app.di.module;

import com.kshrd.dagger2.api.ArticleService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pirang on 9/2/17.
 */

@Module(includes = NetworkModule.class)
public class RestfulModule {

    private static final String BASE_URL = "http://www.api-ams.me/v1/api/";

    @Provides
    public Retrofit provideRetrofit(OkHttpClient okHttpClient){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    @Provides
    public ArticleService provideArticleService(OkHttpClient okHttpClient){
        return provideRetrofit(okHttpClient).create(ArticleService.class);
    }


}
