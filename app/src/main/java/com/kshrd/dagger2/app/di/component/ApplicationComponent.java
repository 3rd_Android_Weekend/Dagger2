package com.kshrd.dagger2.app.di.component;

import com.kshrd.dagger2.MainActivity;
import com.kshrd.dagger2.app.di.module.ApplicationModule;
import com.kshrd.dagger2.app.di.module.PreferenceModule;
import com.kshrd.dagger2.app.di.module.RestfulModule;
import com.kshrd.dagger2.data.PreferenceHelper;

import dagger.Component;

/**
 * Created by pirang on 8/20/17.
 */

@Component(modules = {ApplicationModule.class, PreferenceModule.class, RestfulModule.class})
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);
    PreferenceHelper preferenceHelper();

}
