package com.kshrd.dagger2.app.di.module;

import com.kshrd.dagger2.data.AppPreferenceHelper;
import com.kshrd.dagger2.data.PreferenceHelper;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pirang on 8/20/17.
 */

@Module
public class PreferenceModule {

    @Provides
    public PreferenceHelper providePreferenceHelper(AppPreferenceHelper appPreferenceHelper){
        return appPreferenceHelper;
    }

}
